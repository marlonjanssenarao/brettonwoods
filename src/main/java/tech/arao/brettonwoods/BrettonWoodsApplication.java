package tech.arao.brettonwoods;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrettonWoodsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrettonWoodsApplication.class, args);
	}

}
